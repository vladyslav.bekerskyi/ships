const mongoose = require('mongoose');


const truckSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    default: null
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    enum: ['IS', 'OL'],
    default: 'IS',
  },
  created_date: {
    type: Date,
    required: true,
    default: Date.now
  }
} )


module.exports = mongoose.model('Truck', truckSchema);