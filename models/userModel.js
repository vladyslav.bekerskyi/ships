const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const userSchema = mongoose.Schema({
  role: {
    type: String,
    required: true,
    enum: ['DRIVER', 'SHIPPER']
  },
  email: {
    type: String,
    required: [true, 'Please add an email'],
    unique: true,
  },
  password: {
    type: String,
    required: [true, 'Please add a password'],
  }
});

module.exports = mongoose.model('User', userSchema);