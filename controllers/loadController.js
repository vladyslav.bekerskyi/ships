const asyncHandler = require('express-async-handler');
const Load = require('../models/loadModel');
const Truck = require('../models/truckModel')
const post = require('../utils/postLoad')
const loadlogs = require('../utils/loadlogs')
const User = require('../models/userModel')
const checkRole = require('../utils/checkRoles')


const setLoad = asyncHandler(async (req, res) => {
  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (req.user.role !== "SHIPPER") {
    return res.status(400).json({
      message: `You've no rights. Only shipper can add a load.`
    })
  }


  await Load.create({
    created_by: req.user.id,
    name: req.body.name,
    payload: req.body.payload,
    pickup_address: req.body.pickup_address,
    delivery_address: req.body.delivery_address,
    dimensions: req.body.dimensions
  });

  res.status(200).json({
    message: "Load created successfully",
  });
});


const getLoads = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user.id);
  const status = req.query.status;
  const offset = +req.query.offset || 0;
  const limit = +req.query.limit || 10;

  if (status) {
    if ((user.role === "SHIPPER")) {
      const loads = await Load.find({
          created_by: user.id,
          status,
        })
        .skip(offset)
        .limit(limit);
      return res.status(200).json({
        loads,
      });
    }

    if ((user.role === "DRIVER")) {
      const loads = await Load.find({
          assigned_to: user.id,
          status,
        })
        .skip(offset)
        .limit(limit);
      return res.status(200).json({
        loads,
      });
    }
  }

  if ((user.role === "SHIPPER")) {
    const loads = await Load.find({
        created_by: user.id,
      })
      .skip(offset)
      .limit(limit);
    return res.status(200).json({
      loads,
    });
  }

  if ((user.role === "DRIVER")) {
    const loads = await Load.find({
        assigned_to: user.id,
      })
      .skip(offset)
      .limit(limit);

    return res.status(200).json({
      loads,
    });
  }
});


const getActive = asyncHandler(async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({
      message: `You've no rights. Only driver can check active load.`
    })
  }

  const load = await Load.findOne({
      assigned_to: req.user.id,
      state: {
        $ne: "Arrived to delivery"
      }
    })
    .select('-__v');

    res.status(200).json({
      load: load
    })
});

const nextState = asyncHandler(async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({
      message: `You've no rights. Only driver can check active load.`
    })
  }
  const truck = await Truck.findOne({
    assigned_to: req.user.id,
  })
  const load = await Load.findOne({
      assigned_to: req.user.id,
      state: {
        $ne: "Arrived to delivery"
      }
    })
    .select('-__v');


    if(!load){
      res.status(400);
      throw new Error('No active routes')
    }
  if (load.state === 'En route to Pick Up') {
    load.state = 'Arrived to Pick Up'
    loadlogs(load.logs, 'State changed to "Arrived to Pick Up"')
    await Load.findByIdAndUpdate(load._id, load, {
      new: true,
    })
    return res.status(200).json({
      message: `Load state changed to 'Arrived to Pick Up'`
    })
  }


  if (load.state === 'Arrived to Pick Up') {
    load.state = 'En route to delivery'
    loadlogs(load.logs, 'State changed to "En route to delivery"')
    await Load.findByIdAndUpdate(load._id, load, {
      new: true,
    });
    return res.status(200).json({
      message: `Load state changed to 'En route to delivery'`
    })
  }


  if (load.state === 'En route to delivery') {
    load.state = 'Arrived to delivery'
    loadlogs(load.logs, 'State changed to "Arrived to delivery"')
    load.status = 'SHIPPED'
    loadlogs(load.logs, 'Status changed to "SHIPPED"')

    truck.status = "IS"
    await Load.findByIdAndUpdate(load._id, load, {
      new: true,
    });
    await Truck.findByIdAndUpdate(truck._id, truck,{
      new: true
    })

    return res.status(200).json({
      message: `Load state changed to 'Arrived to delivery'`
    })
  } 


});

const getLoad = asyncHandler(async (req, res) => {
  const load = await Load.findById(req.params.id);

  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (load.created_by.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  res.status(200).json({
    load: load
  });
});

const updateLoad = asyncHandler(async (req, res) => {
  if (req.user.role !== "SHIPPER") {
    return res.status(400).json({
      message: `You've no rights. Only shipper can update loads.`
    })
  }
  const load = await Load.findById(req.params.id);

  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (load.created_by.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Load.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  });
  res.status(200).json({
    message: "Load details changed successfully",
  });
});

const deleteLoad = asyncHandler(async (req, res) => {
  if (req.user.role !== "SHIPPER") {
    return res.status(400).json({
      message: `You've no rights. Only shipper can delete loads.`
    })
  }
  const load = await Load.findById(req.params.id);

  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (load.created_by.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Load.findByIdAndDelete(req.params.id);
  res.status(200).json({
    message: "Load deleted successfully",
  });
});



const postLoad = asyncHandler(async (req, res) => {
  if (req.user.role !== "SHIPPER") {
    return res.status(400).json({
      message: `You've no rights. Only shipper can post loads.`
    })
  }
  const load = await Load.findById(req.params.id);

  const trucks = await Truck.find({
    assigned_to: {
      $ne: null
    },
    status: "IS"
  });

  let truck;

  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (load.created_by.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }


  if (!trucks.length) {
    res.status(400);

    throw new Error('There is no suitable trucks')
  }

  if (load.status !== "NEW") {
    res.status(400);

    throw new Error('Load is already posted')
  }


  loadlogs(load.logs, 'Load posted successfully')


  if (load.dimensions.width <= 300 && load.dimensions.length <= 250 &&
    load.dimensions.length < 170 && load.payload < 1700) {
    truck = trucks.find(obj => {
      return obj.type === 'SPRINTER'
    })


    post(load, truck)
    await Load.findByIdAndUpdate(req.params.id, load, {
      new: true,
    });
    await Truck.findByIdAndUpdate(truck._id, truck)
    return res.status(200).json({
      "message": "Load posted successfully",
      "driver_found": true
    });
  }

  if (load.dimensions.width <= 500 && load.dimensions.length <= 250 &&
    load.dimensions.length <= 170 && load.payload <= 2500) {
    truck = trucks.find(obj => {
      return obj.type === 'SMALL STRAIGHT'
    })

    post(load, truck)
    await Load.findByIdAndUpdate(req.params.id, load, {
      new: true,
    });
    await Truck.findByIdAndUpdate(truck._id, truck)
    return res.status(200).json({
      "message": "Load posted successfully",
      "driver_found": true
    });
  }


  if (load.dimensions.width <= 700 && load.dimensions.length <= 350 &&
    load.dimensions.length <= 200 && load.payload <= 4000) {
    truck = trucks.find(obj => {
      return obj.type === 'LARGE STRAIGHT'
    })

    post(load, truck)
    await Load.findByIdAndUpdate(req.params.id, load, {
      new: true,
    });
    await Truck.findByIdAndUpdate(truck._id, truck)
    return res.status(200).json({
      "message": "Load posted successfully",
      "driver_found": true
    });
  } else {
    load.status='NEW'
    loadlogs(load.logs, 'Load status again "NEW"')
    await Load.findByIdAndUpdate(req.params.id, load, {
      new: true,
    });
    res.status(400);
    throw new Error('There is no suitable trucks')
  }
});



const getLoadShippingById = asyncHandler(async (req, res) => {
  const load = await Load.findById(req.params.id);
  const truck = await Truck.findOne({
    assigned_to: load.assigned_to
  });
  

  const user = await User.findById(req.user.id);

  checkRole(user.role, "SHIPPER", res);

  // check for user
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  if (load.created_by.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  res.status(200).json({
    load,
    truck: truck,
  });
});



module.exports = {
  setLoad,
  getLoads,
  getActive,
  getLoad,
  nextState,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadShippingById
}