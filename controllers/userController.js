const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');

const registerUser = asyncHandler(async (req, res) => {
  const {email, password, role} = req.body;
  if (!email || !password || !role) {
    res.status(400);
    throw new Error('Please add all fields');
  }

  const userExist = await User.findOne({
    email,
  });

  if (userExist) {
    res.status(400);
    throw new Error('User already exists');
  }

  // Hash password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);

  const user = await User.create({
    email,
    password: hashedPassword,
    role,
  });

  if (user) {
    res.status(200).json({
      message: 'Profile created successfully',
    });
  } else {
    res.status(400);
    throw new Error('Invalid user data');
  }
});

const loginUser = asyncHandler(async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({
    email,
  });
  if(!user){
    res.status(400);
    throw new Error('Invalid credential');
  }

  const token = generateToken(user._id);

  if (user && (await bcrypt.compare(password, user.password))) {
    res.json({
      message: 'Success',
      jwt_token: token,
    });
  } else {
    res.status(400);
    throw new Error('Invalid credential');
  }
});

const getMe = asyncHandler(async (req, res) => {
  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  const {_id, email, createdAt} = await User.findById(req.user.id);

  if (_id.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  res.status(200).json({
    user: {
      _id: _id,
      email,
      createdDate: createdAt,
    },
  });
});

const deleteMe = asyncHandler(async (req, res) => {
  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }
  const {_id} = await User.findById(req.user.id);

  if (_id.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await User.findByIdAndDelete(_id);

  res.status(200).json({
    message: 'Success',
  });
});

const changePassword = asyncHandler(async (req, res) => {
  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  const user = await User.findById(req.user.id);
  const {oldPassword, newPassword} = req.body;

  if (user._id.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  if (await bcrypt.compare(oldPassword, user.password)) {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(newPassword, salt);
    user.password = hashedPassword;
  } else {
    res.status(400);
    throw new Error('Invalid credential');
  }

  await User.findByIdAndUpdate(user._id, user, {
    new: true,
  });

  res.status(200).json({
    message: 'Success',
  });
});

const generateToken = (id) => {
  return jwt.sign(
      {
        id,
      },
      process.env.JWT_SECRET,
      {
        expiresIn: '30d',
      },
  );
};

module.exports = {
  registerUser,
  loginUser,
  getMe,
  deleteMe,
  changePassword,
};
