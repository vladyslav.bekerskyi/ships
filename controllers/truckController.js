const asyncHandler = require('express-async-handler');
const Truck = require('../models/truckModel');

const typesOfTrucks = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT']



const setTruck = asyncHandler(async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({
      message: `You've no rights. Only driver can add trucks.`
    })
  }
  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (!req.body.type) {
    res.status(400);
    throw new Error('Please add a type od a truck');
  }

  if (!req.body.type in typesOfTrucks) {
    res.status(400);
    throw new Error('Not existing type of truck')

  }

  await Truck.create({
    created_by: req.user.id,
    type: req.body.type,
    status: 'IS',
  });

  res.status(200).json({
    message: 'Success',
  });
});


const getTrucks = asyncHandler(async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({
      message: `You've no rights. Only driver can get trucks.`
    })
  }
  const trucks = await Truck.find({
      userId: req.user.id,
    })
    .select('-__v');

  res.status(200).json({
    trucks: trucks,
  });
});


const getTruck = asyncHandler(async (req, res) => {
  const truck = await Truck.findById(req.params.id);
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({
      message: `You've no rights. Only driver can get truck by id.`
    })
  }

  if (!truck) {
    res.status(400);
    throw new Error('Truck not found');
  }

  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (truck.created_by.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  res.status(200).json({
    truck: {
      _id: truck._id,
      created_by: truck.created_by,
      assigned_to: truck.assigned_to,
      type: truck.type,
      status: truck.status,
      created_date: truck.created_at,
    },
  });
});

const updateTruck = asyncHandler(async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({
      message: `You've no rights. Only driver can update truck by id.`
    })
  }
  const truck = await Truck.findById(req.params.id);

  if (!truck) {
    res.status(400);
    throw new Error('Note not found');
  }

  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (truck.status === 'ON') {
    res.status(400);
    throw new Error('Truck is on load');
  }

  if (truck.created_by.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Truck.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  });
  res.status(200).json({
    message: "Truck details changed successfully",
  });
});

const deleteTruck = asyncHandler(async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({
      message: `You've no rights. Only driver can delete truck by id.`
    })
  }
  const truck = await Truck.findById(req.params.id);

  if (truck.status === 'ON') {
    res.status(400);
    throw new Error('Truck is on load');
  }

  if (!truck) {
    res.status(400);
    throw new Error('Truck not found');
  }

  if (!req.user) {
    res.status(400);
    throw new Error('User not found');
  }

  if (truck.created_by.toString() !== req.user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Truck.findByIdAndDelete(req.params.id);
  res.status(200).json({
    message: "Truck deleted successfully",
  });
});

const assignTruck = asyncHandler(async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({
      message: `You've no rights. Only driver can assign truck.`
    })
  }
  const truck = await Truck.findById(req.params.id);
  const trucks = await Truck.find({
    create_by: req.user.id,
  })
  const validation =[]
  trucks.forEach((item) => {
    if(item.assigned_to){
      validation.push(item.assigned_to.toString())
    }
  })
  if (validation.includes(req.user.id)) {
    return res.status(400).json({
      message: `This driver is already busy`
    })
  }


  if (truck.assigned_to!==null) {
    return res.status(400).json({
      message: `Truck is already assigned`
    })
  }

  truck.assigned_to = req.user.id

  await Truck.findByIdAndUpdate(req.params.id, truck, {
    new: true,
  });

  res.status(200).json({
    message: "Truck successfully assigned",
  });

})


module.exports = {
  getTrucks,
  setTruck,
  getTruck,
  assignTruck,
  deleteTruck,
  updateTruck
}