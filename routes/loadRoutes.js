const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {protect} = require('../middleware/authMiddleware');
const {setLoad, getLoads, getActive, getLoad, nextState, updateLoad, deleteLoad, postLoad,
  getLoadShippingById} = require('../controllers/loadController')

router.route('/').get(protect, getLoads).post(protect, setLoad)
router.route('/active').get(protect, getActive)
router.route('/:id').get(protect, getLoad).put(protect, updateLoad).delete(protect, deleteLoad).post()
router.route('/:id/post').post(protect, postLoad)
router.route('/:id/shipping_info').get(protect, getLoadShippingById)

router.route('/active/state').patch(protect, nextState)






module.exports = router;