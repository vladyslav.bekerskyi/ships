const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {registerUser, loginUser} = require('../controllers/userController');

router.post('/register', registerUser);
router.post('/login', loginUser);

module.exports = router;
