const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {
  getMe,
  deleteMe,
  changePassword,
} = require('../controllers/userController');
const {
  protect
} = require('../middleware/authMiddleware');
router
  .route('/me')
  .get(protect, getMe)
  .patch(protect, changePassword);

router.route('/me/password').delete(protect, deleteMe)

module.exports = router;