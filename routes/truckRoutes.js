const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {
  protect
} = require('../middleware/authMiddleware');
const {
  getTrucks,
  setTruck,
  getTruck,
  assignTruck,
  deleteTruck,
  updateTruck
} = require('../controllers/truckController')

router.route('/').get(protect, getTrucks).post(protect, setTruck);
router
  .route('/:id')
  .put(protect, updateTruck)
  .delete(protect, deleteTruck)
  .get(protect, getTruck);

router.route('/:id/assign').post(protect, assignTruck)

module.exports = router;