const express = require('express');
require('dotenv').config();
const connectDB = require('./config/db');
const errorHandler = require('./middleware/errorHandler');
const morgan = require('morgan');


connectDB();

const app = express();

app.use(express.json());
app.use(
    express.urlencoded({
      extended: false,
    }),
);
app.use(morgan(':method :status :url  :response-time ms'));

app.use('/api/auth', require('./routes/authRoutes'));
app.use('/api/users', require('./routes/userRoutes'));
app.use('/api/trucks', require('./routes/truckRoutes'));
app.use('/api/loads', require('./routes/loadRoutes'));

app.use(errorHandler);

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}...`);
});
