const checkRole = (role, availableRole, response) => {
  if (role !== availableRole) {
    response.status(400);
    throw new Error(`Available only for ${availableRole} role`);
  }
};

module.exports = checkRole;
