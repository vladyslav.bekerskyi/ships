const loadlogs = require('./loadlogs')


const post = function (load, truck) {
  load.assigned_to = truck.assigned_to
  loadlogs(load.logs, `Load is assigned to ${load.assigned_to}`)
  load.status = "ASSIGNED"
  loadlogs(load.logs, `Load status is changed to ${load.status}`)
  truck.status = "OL"
  load.state = "En route to Pick Up"
  loadlogs(load.logs, `Load state is changed to ${load.state}`)

}

module.exports = post